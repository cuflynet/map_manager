#include <cstdlib>  
#include <vector>

#ifndef _goalGen_h_
#define _goalGen_h_

void squarePath(std::vector< std::vector< int > > &goal, std::vector< int > &rg, int &num, int &goalindexstart, int x, int y, int originX, int originY);
//void editGoal(std::vector< std::vector< int > > &goal, std::vector< int > &rg, int x, int y, int z)

#endif
