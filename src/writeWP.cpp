#include <fstream>
#include <cstdlib>  
#include <vector>
#include <iostream>

// -----------------------------------------------------------------------------
  // ** Write Waypoints **
  //  *	Write PATH SELECTION algorithm generated waypoints to text file, 
  //  * input array and # of waypoints as "m"
  // ** (added by Bryce)
  void writeWParray(int Array[][2], int m){
  	std::ofstream file("/home/bryce/catkin_ws/src/map_manager/src/fuck.txt");		// Open .txt file
	file << "x,y,z," << std::endl;
  	for(int i = 0;i < m; i++){  
  		for(int j = 0; j < 2; j++){
			file << Array[i][j];	// Write [Mx(1/2/3)] array.txt file
			file << ",";
		}
		file << "1,";               // zth value, = how high do you want to fly?????
		file << std::endl;
	}
	file.close();				// Close .txt file
  }
  
  // ------------------------------------------------------------
  void writeWPvec(std::vector< std::vector< int > > path, int m){
    //std::cout<< "Entered waypoint write FCN:"<<std::endl;
    //std::cout<< m <<std::endl;
  	std::ofstream file("/home/bryce/catkin_ws/src/map_manager/src/fuck.txt");		// Open .txt file
	file << "x,y,z," << std::endl;
  	for(int i = 0;i < m; i++){  
  		for(int j = 0; j < 2; j++){
  		    //std::cout<< path[i][j] <<std::endl;
			file << path[i][j];	// Write [Mx(1/2/3)] array.txt file
			file << ",";
		}
		file << "1,";               // zth value, = how high do you want to fly?????
		file << std::endl;
	}
	file.close();				// Close .txt file
  }
  
  // ------------------------------------------------------------
  std::vector< std::vector< int > > array2vector(int array[][2], int size){
    //================ Define vector 1 ===============
    std::vector< std::vector< int > > v;
    std::vector< int > r;

    //============== Generate =================
    int h = size;
    int w = 2;
    int count = 0;
    for (unsigned int rowIdx = 0; rowIdx < h; rowIdx++){
        v.push_back(r);
        for(unsigned int colIdx = 0; colIdx < w; colIdx++){
            v[rowIdx].push_back(array[rowIdx][colIdx]);
        }
    }
    return v;
  }
