/*
rePlanTalk
Written by: Bryce Hill
(basically just the standard example)

A publisher to simulate the replan message that controls will be providing
*/

#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rePlanTalker");

  ros::NodeHandle n;

  // "chatter" = topic name
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("replan", 1000);
  
  ros::Rate loop_rate(.1);

  int count = 0;
  while (ros::ok())
  {
    std_msgs::String msg;
    //std_msgs::bool msg;

    std::stringstream ss;

    //std::boolean ss;
    ss << "1";
    //ss = true;
    msg.data = ss.str();
    //msg.data = ss;

    //ROS_INFO("%s", msg.data.c_str());

    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}
