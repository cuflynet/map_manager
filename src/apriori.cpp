#include "ros/ros.h"
#include <cstdlib>
#include <vector>

#include "apriori.h"
#include "struct.h"


  // Generate A priori map (a square 7.3[m]x7.3[m] with resolution = .05[m])using map bounds,
  //  RTAB resolution[m] as input parameters

std::vector< std::vector< int > > genApriori(float resolution){
    aprioriMap aStruct;
    //========== Define Apriori map vectors ===============
    std::vector< std::vector< int > > amap;
    std::vector< int > arow;

    //============== Generate Apriori Map =================
    int height = aStruct.height / resolution;
    int width = aStruct.width / resolution;  
    std::cout << "Apriori height: " << height << std::endl;
    std::cout << "Apriori width: " << width << std::endl;
    
    for (unsigned int arowIdx = 0; arowIdx < height; arowIdx++){
        amap.push_back(arow);
        for(unsigned int acolIdx = 0; acolIdx < width; acolIdx++){
            if(arowIdx == 0 || arowIdx == height -1){
                amap[arowIdx].push_back(1);
            }
            else if(acolIdx == 0 || acolIdx == width -1){
                amap[arowIdx].push_back(1);
            }
            else amap[arowIdx].push_back(0);
        }
    }
    return amap;
}

// This is how to do it if you put it in proj_map_client.cpp where you see <//insert> tag
        /*//========== Define Apriori map vectors ===============
        std::vector< std::vector< int > > amap;
        std::vector< int > arow;
        
        //============== Generate Apriori Map =================
        int height = aMap.height / rMap.res;
        int width = aMap.width / rMap.res;  
        std::cout << "Apriori height: " << height << std::endl;
        std::cout << "Apriori width: " << width << std::endl;
        
        for (unsigned int arowIdx = 0; arowIdx < height; arowIdx++){
            amap.push_back(arow);
            for(unsigned int acolIdx = 0; acolIdx < width; acolIdx++){
                if(arowIdx == 0 || arowIdx == height -1){
	                amap[arowIdx].push_back(1);
                }
                else if(acolIdx == 0 || acolIdx == width -1){
                    amap[arowIdx].push_back(1);
                }
                else amap[arowIdx].push_back(0);
            }
        }*/
