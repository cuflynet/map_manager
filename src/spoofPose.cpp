/*
spoofPose
Written by: Bryce Hill
(basically just the standard example)

A publisher to simulate the pose message from MAVROS
*/

#include "ros/ros.h"
#include <geometry_msgs/Point.h>

#include <sstream>
#include <iostream>
#include <cstdlib>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "spoofPose");

  ros::NodeHandle p;

  // "chatter" = topic name
  ros::Publisher spoof_pub = p.advertise<geometry_msgs::Point>("spoofedPose", 1000);
  
  ros::Rate loop_rate(.11);

  int count = 0;
  while (ros::ok())
  {
    geometry_msgs::Point pose;
    //Origin should be 0, 0
    
    //1st point
    if(count == 0){    
        pose.x = 2;
        pose.y = -2;
        pose.z = 1;
    }
    else if(count == 1){
        pose.x = 2;
        pose.y = 2;
        pose.z = 1;   
    }
    else if(count == 2){
        pose.x = -2;
        pose.y = 2;
        pose.z = 1;  
    }
    else if(count == 3){
        pose.x = -2;
        pose.y = -2;
        pose.z = 1; 
    }
    else if(count == 4){
        pose.x = 2;
        pose.y = -2;
        pose.z = 1; 
        count = 0;
    }


    std::cout << "Pose (relative to origin): "<< pose.x << ", "<< pose.y<< ", "<< pose.z << std::endl;

    spoof_pub.publish(pose);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}
