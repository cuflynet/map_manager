#include "goalGen.h"
#include <cstdlib>
#include <iostream>

#include "struct.h"

void squarePath(std::vector< std::vector< int > > &goal, std::vector< int > &rg, int &num, int &goalindexstart, int x, int y, int originX, int originY){
    //
    //aprioriMap a_map;
    rtabMap r_map;
    float res = 1;
    int h = 4, w = 4;
    // 2[m] path
    float len = 1; 
    // Required # of cells to traverse
    float stepf;
    stepf = len/res;     
    //std::cout<< stepf << std::endl;
    int step = int(stepf);
    //int origin = 5;

    // [x, y]
    goal.push_back(rg);    
    goal[0].push_back(originX);
    goal[0].push_back(originY);

    goal.push_back(rg);    
    goal[1].push_back(originX + step);
    goal[1].push_back(originY);

    goal.push_back(rg);    
    goal[2].push_back(originX + step);
    goal[2].push_back(originY + step);

    goal.push_back(rg);    
    goal[3].push_back(originX);
    goal[3].push_back(originY + step);

    goal.push_back(rg);    
    goal[4].push_back(originX);
    goal[4].push_back(originY);

    // This is here due to the method of indexing in proj_map_client.cpp. It will not be used, 
    //  it simply avoids a segmentation fault
    goal.push_back(rg);    
    goal[5].push_back(originX);
    goal[5].push_back(originY);

    int j = 0;
    int cnt = 0;
    // Check that current location is equal to waypoints, if so, don't plan to those goals
    for(unsigned int i = 0; i < num; i++){
        if(goal[i][0] == x && goal[i][1] == y){
            goalindexstart = i + 1;
            //std::cout<< std::endl << "Goal to erase:" << std::endl;
            //std::cout<< goal[i][0] << ", "<< goal[i][1]<< std::endl;
            //std::cout<< std::endl << "-------------:" << std::endl;
            //goal.erase(goal.begin(), goal.begin() + j);
            
        }
    }
    
}

//void editGoal(std::vector< std::vector< int > > &goal, std::vector< int > &rg, int x, int y, int z){
    
//}
