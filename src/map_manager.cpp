/* 
Map manager node
Written by: Bryce Hill
3/29/16
*/

// Include c/c++ stuff
#include <cstdlib>
#include <cstdio>	// for returning array
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <time.h>
#include <sys/time.h>
// Include ROS stuff
#include <ros/types.h> 
#include "ros/ros.h"
// Include RTABmap stuff
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GetMap.h>
#include <geometry_msgs/Point.h>
#include "std_msgs/String.h"

// Include custom code
#include "Astar.h"
#include "struct.h"
#include "apriori.h"
#include "goalGen.h"
#include "writeWP.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/*
TO-DO
- Include planning algorithm                        [CHECK]
- How to define size of map                         [CHECK] variable sized
- Include topic subscriptions 
	- Make these dictate mains performance          [CHECK]
- Run Astar multiple times, append/write results    [CHECK]
- Goal generation, four points in a square
    - Upgrade to find unkown space adjacent
        to known clear space
    
*/

  // Initialize global map structure defined in struct.h  
  rtabMap rMap;   
  int replanCountReq = 0, replanCountPrev = 0, replanCountTotal = 0;
  int MAPSELECT = 1;    //RTAB =1 APRIORI = 0
  float xpose = 0; // 10
  float ypose = 0; // 5
  float zpose = 0; // 0
  time_t tstart, tend; 
  #define BILLION 1000000000L
  
  // ==============================
  double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
    }
    double get_cpu_time(){
        return (double)clock() / CLOCKS_PER_SEC;
    }
  // ===============================
  
  // ***************************************************************************
  //                              ** Callbacks ** 		
  // ***************************************************************************
  void replanCallback(const std_msgs::String::ConstPtr& msg)
  {
    //ROS_INFO("Replan Requested:");    // i.e. TRUE recieved
    std::cout << std::endl;
    std::cout<<"======= Replan Requested: ======="<< std::endl;
    replanCountReq++;
  }

  void spoofCallback(const geometry_msgs::Point& pose)
  {
    //ROS_INFO("Pose updated: ");
    std::cout<< "Pose updated: " << std::endl;
    //xpose = pose.x;
    //ypose = pose.y;
    //zpose = pose.z;
    std::cout<< "Current Pose = "<< xpose << ", "<< ypose<< ", "<< zpose<< std::endl;
  }


  // ***************************************************************************
  //                                ** MAIN ** 		
  // ***************************************************************************
  int main(int argc, char **argv){
  
  	uint64_t diff; 
  	struct timespec start, end; 
  	struct timespec startcpu, endcpu;
  	int p;
  	
  //=================== ROS Node Init =======================
  // This is where we specify the name of our node, must be unique and
  // 	must be a base name i.e. no "/" can be included
  ros::init(argc, argv, "map_manager");
  // nodeHandle creates a handle this process' node 
  ros::NodeHandle m;

  //=================== ROS Services =======================
  // Create the service client object (srv) to get the 2D map projection from RTABmap

  // Create service client
  //ros::ServiceClient client = m.serviceClient<nav_msgs::GetMap>("2DprojectedMap");
  ros::ServiceClient client = m.serviceClient<nav_msgs::GetMap>("/rtabmap/get_proj_map");  

  // Pick the service (find using "rosservice type /rtabmap/get_proj_map")
  nav_msgs::GetMap srv;
  // Send srv request (in this case, no request parameters)- see api msg/srv documentation
  srv;
  
  //=================== ROS Subscribers =======================
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);
  // Subscribe to controls replan topic 
  ros::Subscriber sub_plan = m.subscribe("replan", 1000, replanCallback);

  // Subscribe to pose topic
  ros::Subscriber sub_pose = m.subscribe("spoofedPose", 1000, spoofCallback);

  // Start the Spinner
  spinner.start();

  // Enter loop, if replan has been triggered execute stuff
  while(ros::ok()){
    if(replanCountReq > replanCountPrev){
    
        //  Start Timers
        double wall0 = get_wall_time();
        double cpu0  = get_cpu_time();
        
        ros::WallTime starttime = ros::WallTime::now();
        
        tstart = time(0);
        
        clock_gettime(CLOCK_MONOTONIC, &start);
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &startcpu);    
                   
        if (client.call(srv)){       
        // Match to map data type: 
        // (find here: http://docs.ros.org/api/nav_msgs/html/index-msg.html)
        
        // Send srv request (in this case, no request parameters)- see api msg/srv documentation
        srv;
        
        // Get dat data yo
        rMap.width = srv.response.map.info.width;
        rMap.height = srv.response.map.info.height;
        rMap.res = srv.response.map.info.resolution;
        rMap.xOrigin = srv.response.map.info.origin.position.x;
        rMap.yOrigin = srv.response.map.info.origin.position.y;  
        }
          else
          {
            ROS_ERROR("Failed to call service __");
            return 1;
          }
        
        //--------- Define origin [in matrix indices(cells)] -------
        int originCellX = -rMap.xOrigin/rMap.res;  
        int originCellY = -rMap.yOrigin/rMap.res;
        int originCell[2] = {originCellX,originCellY};  
        
        //----------- Add origin offset to pose spoofing ----------
        xpose = xpose + originCellX;
        ypose = ypose + originCellY;
        std::cout<< "Current Pose = "<< xpose << ", "<< ypose<< ", "<< zpose<< std::endl;

        //--------------- Define vectors to use --------------------
        std::vector< std::vector< int > > PATHVEC;
        int SIZEVEC = 0;
        std::vector< std::vector< int > > pathvec;
        std::vector< int > r;
        int sizevec = 0;
        std::vector< std::vector< int > > goal;
        std::vector< int > rg;
        int numgoals = 4;
        int goalindexstart = 0;
        
        //====== Goal Gen (move out of loop eventually =============
        //-------------- Call squarePath fcn -----------------------
        std::cout<< std::endl << "Goal Vector:" << std::endl;
        squarePath(goal, rg, numgoals, goalindexstart, xpose, ypose, originCellX, originCellY);
        for(int i = goalindexstart; i < numgoals; i++){
            for(int j = 0; j < 2; j++){
                std::cout<< goal[i][j] << ", ";
            }
            std::cout<< std::endl;
        }
        
        //------------ Set initial start/goal ----------------
        int start[2] = {goal[0][0], goal[0][1]};
        int goali[2] = {goal[1][0], goal[1][1]};
        //std::cout<< std::endl << "_______" <<std::endl;
        //std::cout<<start[0]<<", "<<start[1]<<std::endl;
        //std::cout<<goali[0]<<", "<<goali[1]<<std::endl;


        //================= USE RTAB MAP ===========================
        if(MAPSELECT == 1){
            // -- Print it out for development purps -- 
            std::cout<< "---------------- RTAB INFO -----------------" << std::endl;
            std::cout<<"Width of Array: "<< rMap.width << std::endl;
            std::cout<<"Height of Array: "<< rMap.height << std::endl;
            std::cout<<"Resolution of Grid: "<< rMap.res << std::endl;
            std::cout<<"X Origin: "<< rMap.xOrigin << std::endl;
            std::cout<<"Y Origin: "<< rMap.yOrigin << std::endl; 
            std::cout<< "--------------------------------------------" << std::endl;
            
            //--------------Define RTABmap vectors -----------------
            std::vector< std::vector< int > > rmap;
            std::vector< int > row;

            //------------------- Format Map -----------------------
            int count = 0;
            for(unsigned int rowIdx = 0; rowIdx < rMap.height; rowIdx++){
                rmap.push_back(row);
                for(unsigned int colIdx = 0; colIdx < rMap.width; colIdx++){
                    //std::cout << +srv.response.map.data[count] << ", ";
                    // output map, ('+') is to convert to higher order int to be able to output through cout
                    rmap[rowIdx].push_back(srv.response.map.data[count]);
                    count++;
                }
            } 

            //---------------- Set height/width---------------------
            int height = rMap.height -1;    // Avoid seg fault by not accessing memory out of bounds
            int width = rMap.width -1;      // " not sure why
            
            std::cout<< "--------------------------------------------" << std::endl;
            std::cout<< "---------- Start ASTAR sequence ------------" << std::endl;
            for(unsigned int i = goalindexstart; i < numgoals; i++){
                std::cout << "Goal# "<< i << ": " << goali[0] << ", "<<goali[1];
                //int rcode = AstarB(rmap, aMap.start, aMap.goal, aMap.origin, height, width, pathvec, r, sizevec);
                int rcode = AstarB(rmap, start, goali, originCell, height, width, pathvec, r, sizevec);
            
                start[0] = goal[i+1][0];
                start[1] = goal[i+1][1];
                goali[0] = goal[i+2][0];
                goali[1] = goal[i+2][1];
                PATHVEC.insert(PATHVEC.end(), pathvec.begin(), pathvec.end());
                SIZEVEC += sizevec;
 
                if(rcode==0){
                    //printf("SUCCESS!!!! \n");
                }
                else{
                    printf("FAILURE \n");
                }
            }
        }
          
        //================= USE APRIORI MAP ===========================    
        else if(MAPSELECT == 0){
            //---------- Define Apriori map vectors ---------------      
            std::vector< std::vector< int > > aMap; 
            //-------------- Generate Apriori Map -----------------
            aMap = genApriori(rMap.res);
            //insert
            
            //----------------Plan Path Apriori -------------------
            aprioriMap astruct; // Call aprioriMap struct to get height and width values defined here
                                //  ~ in future should update to put these variables in the launchfile
                                
                
            int height = astruct.height / rMap.res;
            int width = astruct.width / rMap.res;  

            /*
            //------------------- Print Map -----------------------
            for(int h = 0; h < height; h++){
             	for(int w = 0; w < width; w++){
                std::cout << aMap[h][w] << ",";
              	}
                std::cout << std::endl;	  	
            }*/

            for(unsigned int i = goalindexstart; i < numgoals; i++){
                std::cout << "Goal: "<< goali[0] << ", "<<goali[1];
                int rcode = AstarB(aMap, start, goali, originCell, height, width, pathvec, r, sizevec);
            
                start[0] = goal[i+1][0];
                start[1] = goal[i+1][1];
                goali[0] = goal[i+2][0];
                goali[1] = goal[i+2][1];
                PATHVEC.insert(PATHVEC.end(), pathvec.begin(), pathvec.end());
                SIZEVEC += sizevec;

                if(rcode==0){
                    //printf("SUCCESS!!!! \n");
                }
                else{
                    printf("FAILURE \n");
                }
            }
        }
        
        //=============== Write Path .txt ====================
        /*
            //????????????????????????????????????????????????
            // For some reason, writeWPvec() doesn't like vectors
            // Need to clean this up and use the vector to write 
            // waypoints to .txt file
            int tempPath[10][2];
            for(int u = 0; u < SIZEVEC; u++){
                for(int y = 0; y < 2; y++){
                    tempPath[u][y] = PATHVEC[u][y];
                    //std::cout<< tempPath[u][y] <<std::endl;
                }
            }*/
                    
            
        std::cout<< "Writing to .txt file"<< std::endl;
        std::cout<< "Number of Waypoints Written = "<<SIZEVEC<< std::endl;
        for(int h = 0; h < SIZEVEC; h++){
         	for(int w = 0; w < 2; w++){
            std::cout << PATHVEC[h][w] << ",";
          	}
            std::cout << std::endl;	  	
        }
        
        writeWPvec(PATHVEC, SIZEVEC);


        replanCountPrev = replanCountReq;
        replanCountTotal++;
        
        //  Stop timers
        double wall1 = get_wall_time();
        double cpu1  = get_cpu_time();

        std::cout << "Wall Time (gettime) = " << wall1 - wall0 << std::endl;
        std::cout << "CPU Time  (clock) = " << cpu1  - cpu0  << std::endl;
        
        tend = time(0);
        std::cout << "Execution time (difftime) = " << difftime(tend, tstart) << std::endl;
        
        
        ros::WallTime endtime = ros::WallTime::now();
        std::cout << "Execution time (ROS) = " << starttime - endtime << std::endl;
        
        clock_gettime(CLOCK_MONOTONIC, &end);
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &endcpu); 
        
        //diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec; 
        //printf("elapsed time = %llu nanoseconds\n", (long long unsigned int) diff);
        
        //diff = BILLION * (endcpu.tv_sec - startcpu.tv_sec) + endcpu.tv_nsec - startcpu.tv_nsec; 
        //printf("elapsed process CPU time = %llu nanoseconds\n", (long long unsigned int) diff);      
        
    } 
  }    
  return 0;
}

