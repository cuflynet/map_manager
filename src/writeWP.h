#include <cstdlib>  
#include <vector>

#ifndef _writeWP_h_
#define _writeWP_h_

void writeWParray(int Array[][2], int m);
void writeWPvec(std::vector< std::vector< int > > path, int m);
std::vector< std::vector< int > > array2vector(int array[][2], int size);

#endif
