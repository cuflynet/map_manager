// Astar.c
//Tyler Clayton
//updated 3/8/16
#include "Astar.h"
//#include "writeWP.h"
#include <stdbool.h>
    // Added by Bryce Hill
    #include <fstream>
    #include <cstdlib>
    #include <iostream>
    #include "writeWP.h"
    //#include <stdint>

#define TRUE 1
#define FALSE 0

// QUESTIONS:
// -Will it be better to initialize an entire OPEN/CLOSE list, 
//  and leav at -1 value until filled, OR should I dynamically 
//  allocate the memory all the time??

int hueCost(node cur, node goal){
	return abs(cur.x-goal.x)+abs(cur.y-goal.y);
}

//
//
//

int nodeMatch(node a, node b){
	//check if node a matches node b
	if (a.x == b.x){
		if(a.y == b.y){
			return 1;
		}
	}
	return 0;
}

//
//
//

int isListed(int open[],int closed[], int *iOptr, int *iCptr, int nodVal){

	bool cl = FALSE;
	bool op = FALSE;
	int i = 0;
	//check closed
	for(i=0;i<*iCptr;i++){
		if(closed[i]==nodVal){
			cl = TRUE;
		}
	}
	//check open
	for(i=0;i<*iOptr;i++){
		if(open[i]==nodVal){
			op = TRUE;
		}
	}
	//return 2:closed 1:open, 0:neither, -1:both
	if(op && !cl){return 1;}
	if(!op && cl){return 2;}
	if(op && cl){return -1;}
	//else 
	return 0;
}

//
//
//

int findMinCost(node nodes[],int open[],int *iOptr){

	int temp1 = open[0];
	int temp2;
	for(int i=1;i<*iOptr;i++){
		temp2 = open[i];
		if(nodes[temp2].g < nodes[temp1].g){
			temp1 = temp2;
		}
	}
	return temp1;
}

//
//
//

void add2open(int open[], int *iOptr, int newItem){
	//open-points to the memory were the open list is
	//iOptr = next index in open to fill
	//newItem - node index to add to list
	open[*iOptr] = newItem;
	*iOptr = *iOptr+1;
}

//
//
//

void MOVEopen2close(int open[], int closed[], int* iOptr, int* iCptr, int nodVal){
	//open-points to the memory were the open list is
	//iOptr = next index in open to fill
	//closed -closed array
	//iCptr = next index in open to fill
	//nodVal = node value/Index to move from open to close
 //==============================================================
	//find nodVal in open
	for(int i=0;i<*iOptr;i++){
		if(open[i]==nodVal){
			//add to closed
			closed[*iCptr]=nodVal;
			*iCptr = *iCptr+1;//increment ptr

			//overwrite open list
			if(*iOptr==1){
				open[0]=-1;
			}else{
				for(int c=i;c<*iOptr;c++){
					open[c]=open[c+1];
				}
			}	
			*iOptr=*iOptr-1; //decrement the ptr
			break;
		}
	}
}

//
//
//

void MOVEclose2open(int open[], int closed[], int* iOptr, int* iCptr, int nodVal){
	//open-points to the memory were the open list is
	//iOptr = next index in open to fill
	//closed -closed array
	//iCptr = next index in open to fill
	//nodVal = node value/Index to move from open to close
 //==============================================================
	//find nodVal in open
	for(int i=0;i<*iCptr;i++){
		if(closed[i]==nodVal){
			//add to open
			open[*iOptr]=nodVal;
			*iOptr = *iOptr+1;

			//overwrite closed list
			if(*iCptr==1){
				closed[0]=-1;
			}else{
				for(int c=i;c<*iCptr;c++){
					closed[c]=closed[c+1];
				}
			}	
			*iCptr=*iCptr-1; //decrement the ptr
			break;
		}
	}
}

//
//
//

int isPath(int row, int col, int len, int path[][2]){
	for(int i=0; i<len; i++){
		if(row==path[i][1]-1){
			if(col==path[i][0]-1){ //^^^switched indicies around
				return 1;
			}
		}
	}
	return 0;
}

//
//
//

void updateOrigin(int path[][NINE], int origin[NINE],int numPts){
	//path - current path based on top left as origin
	//origin - new origin location (x,y)
	//numPts - number of path points
	for(int i=0;i<numPts;i++){

		 //neg x

			if(path[i][0]==origin[0]){
				path[i][0]=0;
			}else{
				path[i][0]=(path[i][0]-origin[0]);
			}

			if(path[i][1]==origin[1]){
				path[i][1]=0;
			}else{
				path[i][1]=(path[i][1]-origin[1])*-1;
			}
	}	
}

//
//
//

void printMap(std::vector< std::vector< int > > map,int strt[NINE], int gl[NINE], int count, int path[][NINE], int height, int width){
	int row, col, i;
	//int k = 0;
	printf("\n------MAP-----\n");
	for(row=0;row<height;row++){ //^^^width
		//print top boundary
		if(row==0){
				for(i=0;i<width;i++){
					printf("- ");
				}
				printf("- -\n");
		}

		for(col=0;col<width;col++){ //^^^height
		//print left boundary
			if(col==0){printf("| ");}
		//print interior
			if(map[row][col]!=1){
				if(row==strt[1]-1 && col==strt[0]-1){printf("S ");}
				else{
					if(row==gl[1]-1 && col==gl[0]-1){printf("G ");} //switch gl indices around
					// /*
					else{
						if(isPath(row,col,count,path)){
							printf(". ");
						}else{ // /
							printf("  ");
						} //
					} //
				}
			}
		
			if(map[row][col]==1){printf("# ");} //occ
			//if(map[row][col])
		//print right bound
			if(col==width-1){printf("|");} //height
		}
		printf("\n");
	}
	//bottom boundary
	for(i=0;i<width;i++){
		printf("- ");
	}
	printf("- -\n");
}


//
//------------------------>ASTAR ALGORITHM<---------------------------
//

//std::vector< std::vector< int > > AstarB(std::vector< std::vector< int > > map,int strt[NINE], int gl[NINE], int origin[NINE], int height, int width){
int AstarB(std::vector< std::vector< int > > map,int strt[NINE], int gl[NINE], int origin[NINE], int height, int width, std::vector<std::vector<int> > &pathVec, std::vector< int > &r, int &sizeVec){
    printf("\nEntered Astar\n");
	int len = width*height; //total number of nodes
 // put map data into struct format
	 node nodes[len];
	 int tempG;

	 int i,row,col;

	 int start; // holds the index number of the start node in "nodes"
	 int goal; // holds the index number of the goal node in "nodes"

 	i = 0;
	for(row=0;row<height;row++){ //^^^row<width
		for(col=0;col<width;col++){ //^^^^col<height
			nodes[i].x = col+1; //^^^^row+1
			nodes[i].y = row+1; //^^^^col+1
			nodes[i].occ = map[row][col]; //^^^^[row][col]
			nodes[i].f = 0;
			nodes[i].g = 0;
			nodes[i].h = 0;

			// mark start and goal nodes
			if (nodes[i].x == strt[0]){
				if(nodes[i].y == strt[1]){
					start = i;
					//nodes[i].occ=1;
				}
			}

			if (nodes[i].x == gl[0]){
				if(nodes[i].y == gl[1]){
					goal = i;
				}
			}
			//increment node counter
			i++;
		}
	}
// allocate memory for closed&open lists, set at maximum amount
	int closed[width*height];
	int open[width*height];
//create pointer index to locate place in open/closed arrays
	int iO = 0; //open index
	int iC = 0; //closed index

//starting node g value equals 0
	nodes[start].g = 0;
	nodes[start].h = hueCost(nodes[start],nodes[goal]);
	nodes[start].f = nodes[start].h;

// the openset starts with the start node
	add2open(open,&iO,start);
//current index of point under consideration, begins with start node
	int cur = start;
	int neigh = -1; //default set up of neighbor node #

//flag to mark when to stop ie reached goal
	bool flag = FALSE;
//int used for return of isListed
	int k = 0;
	int step = 1;
//begin search loop
	while(open[0]!= -1){
		if (nodeMatch(nodes[cur], nodes[goal])){ // end if goal is reached
			flag = TRUE;
			break;
		};
		// search and get neighbors in cross pattern, NOT diagonals
		// move current to closed
		MOVEopen2close(open,closed,&iO,&iC,cur);
	//search neighbors
		for (step=1;step<=4;step++){ //1-up,2-down, 3-right, 4-left
			neigh = -1;

			switch(step) {
				case 1: //move up
					neigh = cur-width;//^^^
					if(neigh<0){
						continue;
					} //out of map,skip node
					break;

				case 2: //move down
					neigh = cur+width;//^^^^
					if(neigh>(len-1)){
						continue;
					} //out of map, skip node
					break;

				case 3: //move right
					neigh = cur+1;
					if(neigh>(width*nodes[cur].y-1)){//^^^^height *.x
						continue;
					} //out of map, skip node
					break;

				case 4: //move left
					neigh = cur-1;
					if(neigh<width*(nodes[cur].y-1)){//^^^^
						continue;
					} //out of map, skip node
					break;
			}

			//check if occupied, if yes skip
			if(nodes[neigh].occ==1){
				continue;
			}
			// calculate g
			tempG = nodes[cur].g+1;

			//if node is already on open list with lower f, skip
			//otherwise add to open
			//if node is on closed list with lower f, skip,
			//otherwise add back to open list with new parent
			k = isListed(open, closed, &iO, &iC, neigh);

			if(k==0){ //not on open or closed
				add2open(open, &iO, neigh);
					nodes[neigh].parent = cur;
					nodes[neigh].g = tempG;
					nodes[neigh].h = hueCost(nodes[neigh],nodes[goal]);
					nodes[neigh].f = tempG+nodes[neigh].h;
			}

			if(k==1){//on open, not closed
					if(tempG<nodes[neigh].g){
						nodes[neigh].parent = cur;
						//nodes[neigh].g = temp.g;
						//nodes[neigh].h = temp.h;
						//nodes[neigh].f = temp.f;
					}
			}

			if(k==2){//on closed, not open
				if(tempG<nodes[neigh].g){
					MOVEclose2open(open,closed,&iO,&iC,neigh);
					nodes[neigh].parent = cur;
					//nodes[neigh].g = temp.g;
					//nodes[neigh].h = temp.h;
					//nodes[neigh].f = temp.f;
				}
			}
		} //end for (loop over current neighbors)

		cur = findMinCost(nodes,open,&iO);
	} // end while (loop until open empty)

// find the path by tracing the parents
	int count = 0;
	if(flag==FALSE){
		printf("\nOpen list is empty\n");
		return(-1);

	}else{
		//print path to screen
		i = -1;
		while(i!=start){
			if(i==-1){
				//printf("------PATH------\n");
				//printf("%d, %d\n",nodes[cur].x, nodes[cur].y);
				i = nodes[cur].parent;
				//count++;
			}else{
				//printf("%d, %d\n",nodes[i].x,nodes[i].y);
				i = nodes[i].parent;
				count++;
			}
		}
		//printf("%d, %d\n",nodes[start].x, nodes[start].y);
		//printf("----END PATH----\n");
	}

	int path[count][NINE];
	k = nodes[cur].parent;
	for(i=count;i>0;i--){
		path[i-1][0] = nodes[k].x;
		path[i-1][1] = nodes[k].y;
		k = nodes[k].parent;
	}
	path[i][0] = gl[0];
	path[i][1] = gl[1];
	count++;
// print map for dev purposes
	//printMap(map,strt,gl,count,path);
//adjust for a new origin located at some pt [o.x,o.y]
	updateOrigin(path,origin,count);

//print path

    printf("------PATH------\n");
	//printf("  %d, %d\n",strt[0], strt[1]);
	for(i=0;i<count;i++){
		printf("  %d, %d\n",path[i][0], path[i][1]);
	}
	//printf("  %d, %d\n",gl[0], gl[1]);
	printf("----END PATH----\n");

    
    //=================== Save Path ======================= (Bryce)
    pathVec.clear();            // Clear the vector so it can be rewritten
    // Get number of waypoints
    int size = sizeof path / sizeof path[0];
    size++;
    // Save to vector in main for appending paths from goal to goal
    for(unsigned int rowIdx = 0; rowIdx < size; rowIdx++){
        pathVec.push_back(r);
        for(unsigned int colIdx = 0; colIdx < 2; colIdx++){
            pathVec[rowIdx].push_back(path[rowIdx][colIdx]);
        }
    } 
    sizeVec = size;

	return 0;
}


