//Astar.h
//Tyler Clayton
//header file for Astar algorithm
//updated 3/8/2016

#ifndef _Astar_h_
#define _Astar_h_

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
//#include <cstdlib>
#include <cstdint>

#define NINE 2 // Haha Tyler - Bryce

typedef struct node{
	int parent; 	//index of parent node
	int x,y;	    //x,y values of node
	int f, g, h;	//cost values of node
	int occ;	    //occupancy status of node
}node;

int hueCost(node cur, node goal);
int nodeMatch(node a, node b);
void add2open(int open[], int* iOptr, int newItem);
void MOVEopen2close(int open[], int closed[], int* iOptr, int* iCptr, int nodVal);
void MOVEclose2open(int open[], int closed[], int* iOptr, int* iCptr, int nodVal);
int isPath(int row, int col, int len, int path[][NINE]);
int isListed(int open[],int closed[], int*iOptr, int *iCptr, int nodVal);
int findMinCost(node nodes[],int open[],int *iOptr);

//std::vector< std::vector< int > > AstarB(std::vector< std::vector< int > > map, int strt[], int gl[],int origin[], int height, int width);
int AstarB(std::vector< std::vector< int > > map, int strt[], int gl[],int origin[], int height, int width, std::vector<std::vector<int> > &pathVec, std::vector< int > &r, int &sizeVec);
void printMap(std::vector< std::vector< int > > map,int strt[], int gl[], int count, int path[][NINE], int height, int width);

#endif
