#include <cstdlib>
#include <vector>  

#ifndef _apriori_h_
#define _apriori_h_

std::vector< std::vector< int > > genApriori(float resolution);

#endif

