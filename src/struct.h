#include <cstdlib>  

#ifndef _struct_h_
#define _struct_h_

  // Define a struct data type to store the data
typedef struct rtabMap{
	int dat, width, height; 
  	float res, xOrigin, yOrigin; 
    // 7.3[m] =~ 24 ft
}rmap;  

typedef struct aprioriMap{
    float width = 7.3, height = 7.3;
    int start[2] = {5,5};
    int goal[2] = {100,100};
    int origin[2] = {5,5};
}apriori;

struct rtabMap getData(int argc, char **argv);

#endif

 /*//This was a demo for returning structs from fcns
  rtabMapTwoD getData(){
	rtabMapTwoD rmap;
	return rmap;
  }*/
