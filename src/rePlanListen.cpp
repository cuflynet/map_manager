/*
rePlanListen
Written by: Bryce Hill

A subscriber to listen for replan message that controls will be providing
*/

#include "ros/ros.h"
#include "std_msgs/String.h"

void replanCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("Replan Requested:");    // i.e. TRUE recieved
  
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rePlanListen");

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("replan", 1000, replanCallback);

  ros::spin();

  return 0;
}
